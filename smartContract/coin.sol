pragma solidity ^0.4.24;
//не доделано
contract EPayTokens {
    
    address owner;
    
    string public constant name = "EPay Tokens";
    string public constant symbol = "EPT";
    uint8 public constant decimals = 0;
    uint256 public totalSupply = 1000;
    
    mapping(address => uint256) balances;
    
    constructor() {
        owner = msg.sender;
    }
    
    function balanceOf(address _tokenOwner) public constant returns (uint) {
        return balances[_tokenOwner];
    }
    
    function transfer(address _to, uint256 _value) public returns (bool) {
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
        return true;
    }
    
    function transferFrom(address _from, address _to, uint _value) public returns (bool) {
        balances[_from] -= _value;
        balances[_to] += _value;
        emit Transfer(_from, _to, _value);
        return true;
    }
    
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}