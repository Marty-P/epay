pragma solidity ^0.4.21;
contract EPayPremium{
    
    address owner;
    uint256 public priceWEI;
    uint256 public periodDAY;
    string public constant name = "EPay Premium";
    mapping (address => uint) premiumAccounts;
    
    function EPayPremium(uint256 _priceWEI, uint256 _periodDAY) public {
        priceWEI = _priceWEI;
        periodDAY = _periodDAY;
        owner = msg.sender;
    }
    
    function() external payable  {
        uint256 amtDays = getNumDays(msg.value);
        owner.transfer(msg.value);
        setPremium(msg.sender, amtDays);
    }
    
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    

    function buyPremium(address _account) external payable {
        uint256 amtDays = getNumDays(msg.value);
        owner.transfer(msg.value);
        setPremium(_account, amtDays);
    }
    
    function setPremium(address _account, uint256 _amtDays) private {
        if(_amtDays > 0) {
            if(isPremium(_account)) {
                premiumAccounts[_account] += _amtDays * 1 days;
            }
            else {
                premiumAccounts[_account] = now + _amtDays * 1 days;
            }
        }
    }
    
    function getNumDays(uint256 _ethereum) private view returns (uint256) {
        uint amtDays = (_ethereum / priceWEI) * periodDAY;
        return amtDays;
    }
    
    function isPremium(address _account) public view returns (bool){
        if(premiumAccounts[_account] > now){
            return true;
        }
        else {
            return false;
        }
    }
    
    function getEndPremium(address _account) public view returns (uint256){
        return premiumAccounts[_account];
    }
    
    function setPrice(uint256 _priceWEI) public onlyOwner {
        priceWEI = _priceWEI;
    }
    
    function setPeriod(uint256 _periodDAY) public onlyOwner {
        periodDAY = _periodDAY;
    }
    
    function setValues(uint256 _priceWEI, uint256 _periodDAY) public onlyOwner {
        priceWEI = _priceWEI;
        periodDAY = _periodDAY;
    }
    
    function setPremiumADMIN(address _account, uint256 _amtDays) public onlyOwner {
        setPremium(_account, _amtDays);    
    }

}