/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./_source_frontend/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./_source_frontend/js/changeOptions.js":
/*!**********************************************!*\
  !*** ./_source_frontend/js/changeOptions.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("window.changeOptions = function (operation) {\r\n  let lastId = parseInt(document.querySelector(\"#numOpts\").value);\r\n  switch(operation) {\r\n    case '+':\r\n      if(lastId < 5) {\r\n        let nextId = lastId + 1;\r\n        document.querySelector(\"#numOpts\").value = nextId;\r\n        let input = document.createElement('input');\r\n        input.setAttribute('id', 'opt' + nextId.toString());\r\n        input.setAttribute('name', 'opt' + nextId.toString());\r\n        input.setAttribute('placeholder',  nextId.toString() + ' значение в Wei');\r\n        input.setAttribute('class', 'input-field input-field-opt');\r\n        document.querySelector(\"#option-fills-area\").appendChild(input);\r\n      }\r\n      break;\r\n    case '-':\r\n      if(lastId > 2) {\r\n        document.querySelector(\"#opt\" + lastId.toString()).remove();\r\n        document.querySelector(\"#numOpts\").value =  lastId - 1;\r\n      }\r\n      break;\r\n  }\r\n\r\n}\n\n//# sourceURL=webpack:///./_source_frontend/js/changeOptions.js?");

/***/ }),

/***/ "./_source_frontend/js/downloadKeyStone.js":
/*!*************************************************!*\
  !*** ./_source_frontend/js/downloadKeyStone.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("window.downloadKeyStone = function (){\r\n  var type = 'data:application/octet-stream;base64, ';\r\n  var keyStone =  document.querySelector(\"#keystone\").value;\r\n  var base = btoa(keyStone);\r\n  document.getElementById('downloadLink').href = type + base;\r\n}\n\n//# sourceURL=webpack:///./_source_frontend/js/downloadKeyStone.js?");

/***/ }),

/***/ "./_source_frontend/js/selectItem.js":
/*!*******************************************!*\
  !*** ./_source_frontend/js/selectItem.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("window.selectItem = function (id) {\r\n  if(id == 'payment-options'){\r\n\r\n    switch(document.querySelector(\"#\"+id).value) {\r\n      case 'fix':\r\n        document.querySelector('#fix').style.display = 'initial';\r\n        document.querySelector('#options').style.display = 'none';\r\n        document.querySelector('#custom').style.display = 'none';\r\n        break;\r\n      case 'options':\r\n        document.querySelector('#fix').style.display = 'none';\r\n        document.querySelector('#options').style.display = 'initial';\r\n        document.querySelector('#custom').style.display = 'none';\r\n        break;\r\n      case 'custom':\r\n        document.querySelector('#fix').style.display = 'none';\r\n        document.querySelector('#options').style.display = 'none';\r\n        document.querySelector('#custom').style.display = 'initial';\r\n        break;\r\n      default:\r\n        document.querySelector('#fix').style.display = 'initial';\r\n        document.querySelector('#options').style.display = 'none';\r\n        document.querySelector('#custom').style.display = 'none';\r\n    }\r\n\r\n  }\r\n  else\r\n    document.querySelector(\"#\"+id).disabled = !document.querySelector(\"#\"+id).disabled;\r\n}\n\n//# sourceURL=webpack:///./_source_frontend/js/selectItem.js?");

/***/ }),

/***/ "./_source_frontend/main.js":
/*!**********************************!*\
  !*** ./_source_frontend/main.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _js_downloadKeyStone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/downloadKeyStone */ \"./_source_frontend/js/downloadKeyStone.js\");\n/* harmony import */ var _js_downloadKeyStone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_js_downloadKeyStone__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_selectItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/selectItem */ \"./_source_frontend/js/selectItem.js\");\n/* harmony import */ var _js_selectItem__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_selectItem__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _js_changeOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/changeOptions */ \"./_source_frontend/js/changeOptions.js\");\n/* harmony import */ var _js_changeOptions__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_js_changeOptions__WEBPACK_IMPORTED_MODULE_2__);\n\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./_source_frontend/main.js?");

/***/ })

/******/ });