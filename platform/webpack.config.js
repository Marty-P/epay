const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: {
      bundle: './_source_frontend/main.js',
      epay: './_source_frontend/scss/epay.scss',
      epay_style: './_source_frontend/scss/epay_style.scss'
    },
    output: {
      path: path.resolve(__dirname, 'template/static/js'),
      filename: '[name].js',
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader']
          }),
          
        }
      ]
    },
    plugins: [
        new ExtractTextPlugin('../css/[name].css'),
        new BrowserSyncPlugin({
          // browse to http://localhost:3000/ during development,
          // ./public directory is being served
          host: '127.0.0.1',
          port: 3000,
          //server: { baseDir: ['template/static/'] },
          files: ['./template/'],
          proxy: "http://127.0.0.1:5000/"
        })
    ]
};