const MYSQL = require('mysql')

function Database(config){
    //this.mySqlPool = MYSQL.createPool(config);
    this.mySqlPool = MYSQL.createPool({
        host: '127.0.0.1',
        port: 3306,
        database: 'EPay',
        user: 'epay',
        password: '1234567890',
        connectionLimit: 10
    });
}

Database.prototype.query = function(request, array) {
    return new Promise((resolve, reject) => {
        this.mySqlPool.query(request, array, (error, results, fields) =>{
            if(error)
                reject(error)
            else
                resolve(results, fields)
        });
    })
}

module.exports = Database;