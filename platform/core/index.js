const EMAIL_SENDER = require('./emailSender')
const DATABASE = require('./database');
const WEB3 = require('web3');
var net = require('net');

function Core(config){
  this.address = config.forntServer;
  this.emailSender = new EMAIL_SENDER(config.serverEMail);
  this.database = new DATABASE(config.serverMySQL);
  //this.web3 = new WEB3(new WEB3.providers.HttpProvider(config.gethServer));
  this.web3 = new WEB3(new WEB3.providers.IpcProvider('/home/Marty/.geth/devNode/geth.ipc', net));
}

module.exports = Core;