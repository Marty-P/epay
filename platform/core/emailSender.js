const nodemailer = require('nodemailer');

function EMailSender(options){
    this.host = options.host;
    this.port = options.port;
    this.secure = options.secure;
    this.account = {
        name: options.account.name,
        user: options.account.user,
        password: options.account.password
    }

    this.transporter = nodemailer.createTransport({
        host: this.host,
        port: this.port,
        secure: this.secure,
        auth: {
            user: this.account.user,
            pass: this.account.password
        }
    });
}

EMailSender.prototype.send = function(to, subject, text, html){
    return new Promise((resolove, reject) => {
        this.transporter.sendMail({
            from: this.account.name + ' <' + this.account.user + '>',
            to: to,
            subject: subject,
            text: text,
            html: html || ''
        }, (error, info) => {
            if(error) 
                reject(error)
            else
                resolove(true)
        });
    });
}

module.exports = EMailSender;