const MODULE_NAME = 'MODEL getPricePremium';

function getPricePremium(unit) {
  return new Promise(async (resolove, reject) => {
    try {
      let price = await this.global.contract.methods.priceWEI().call();

      if(unit != undefined)
        price = await this.global.web3.utils.fromWei(price, unit);

      resolove({
        value: price,
        unit: unit || 'wei'
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  })
};

module.exports = getPricePremium;