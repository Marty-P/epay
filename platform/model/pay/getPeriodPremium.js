const MODULE_NAME = 'MODEL getPeriodPremium';

function getPeriodPremium() {
  return new Promise(async (resolove, reject) => {
    try {
      resolove(await this.global.contract.methods.periodDAY().call())
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  })
};

module.exports = getPeriodPremium;