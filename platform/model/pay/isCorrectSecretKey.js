const MODULE_NAME = 'MODEL - isCorrectSecretKey';

function isCorrectSecretKey(secretKeySrc) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];
      let secretKey = '';

      if(secretKeySrc == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'undefinedSecretKey')); 
      }
      else if(secretKeySrc == "") {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'undefinedSecretKey')); 
      }
      else {
        let checkSecretKey = secretKeySrc.match(/0x[A-Za-z0-9]{64}/);

        if(checkSecretKey == null)
          isValid = false;
        else if(checkSecretKey[0] != checkSecretKey.input)
          isValid = false;
        
        if(!isValid)
          errors.push(this.global.messages.getError('pay', 'notCorrectSecretKey'));
        else {
          secretKey = this.global.common.getEscapeStr(secretKeySrc);
          
        }

      }

      resolove({
        isValid: isValid,
        value: secretKey,
        errors: errors
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectSecretKey;