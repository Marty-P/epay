const MODULE_NAME = 'MODEL - isCorrectPeriod';
const NUM_DAYS_PREMIUM = require('../../data/numOptiunsOfDaysForPremium');

function isCorrectPeriod(period) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if(period == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'notCorrectPeriod')); 
      }
      else if(period == "") {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'notCorrectPeriod')); 
      }
      else {
        let flag = false;
        for(let i in NUM_DAYS_PREMIUM) {
          if(NUM_DAYS_PREMIUM[i] == period)
            flag = true;
            break;
        }

        isValid = flag;
        
        if(!isValid)
          errors.push(this.global.messages.getError('pay', 'notCorrectPeriod'));

      }

      resolove({
        isValid: isValid,
        value: period,
        errors: errors
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectPeriod;