function Pay(global) {
  this.global = global;
}

Pay.prototype.premium = require('./premium');
Pay.prototype.staticPayment = require('./staticPayment');

Pay.prototype.getPricePremium = require('./getPricePremium');
Pay.prototype.getPeriodPremium = require('./getPeriodPremium');

Pay.prototype.isCorrectSecretKey = require('./isCorrectSecretKey');
Pay.prototype.isCorrectPeriod = require('./isCorrectPeriod');
Pay.prototype.isCorrectTotal = require('./isCorrectTotal');

module.exports = Pay;