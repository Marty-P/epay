const MODULE_NAME = 'MODEL staticPayment';


function staticPayment(id, privateKeySrc, totalSrc) {
  return new Promise(async (resolove, reject) => {
    try {
      let errors = [];
      
      let dataOfStaticIntegration = await this.global.platform.getDataOfStaticIntegration(id);

      let total = await this.isCorrectTotal(totalSrc);
      errors = errors.concat(total.errors);
      
      if(total.isValid)
        switch(dataOfStaticIntegration.settings.typePayment){
          case 'fix':
            if(total.value != dataOfStaticIntegration.values)
              errors.push(this.global.messages.getError('pay', 'notCorrectTotal'));
              break;
          case 'options':
            let flag = false;
            for(let i in dataOfStaticIntegration.values) {
              if(dataOfStaticIntegration.values[i] == total.value) {
                flag = true;
                break;
              }
            }
            if(!flag)
              errors.push(this.global.messages.getError('pay', 'notCorrectTotal'));
            break;
        }


      let privateKay = await this.isCorrectSecretKey(privateKeySrc);
      errors = errors.concat(privateKay.errors);

      if((privateKay.isValid) && (errors.length == 0)) {
        let address = await this.global.wallet.getAddressFromPrivateKey(privateKay.value);
        let ballance = await this.global.wallet.getBalance(address);
        
        if(total.isValid){
          if(ballance.value <= total.value)
            errors.push(this.global.messages.getError('pay', 'insufficientFunds'));
        }
      }

      
      if(errors.length == 0) {
        let owner = await this.global.platform.getOwnerFromPaymentPage(id);
        if(!owner.status)
          errors.push(this.global.messages.getError('global', 'unknow'));
        else {
          
          let transaction = await this.global.transaction.send(privateKay.value, {
            to: dataOfStaticIntegration.walletAddress,
            value: total.value,
            gas: 21000
          }, owner.value, dataOfStaticIntegration.platfomTitle, dataOfStaticIntegration.name, );
          errors = errors.concat(transaction.errors);
        }   
      }

      if(errors.length == 0) {
        resolove({
          status: true,
          errors: []
        })
      }
      else {
        resolove({
          status: false,
          errors: errors
        })
      }
        

    
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = staticPayment;