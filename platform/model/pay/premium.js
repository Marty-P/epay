const MODULE_NAME = 'MODEL premium';


function premium(secretKeySrc, periodSrc, gasPrice) {
  return new Promise(async (resolove, reject) => {
    try {
      let errors = [];

      let pricePremium = await this.getPricePremium('wei');

      let secretKey = await this.isCorrectSecretKey(secretKeySrc);
      errors = errors.concat(secretKey.errors);

      let period = await this.isCorrectPeriod(periodSrc);
      errors = errors.concat(period.errors);

      if(errors.length == 0) {
        let userData = await this.global.account.getSessionData();
        let methodsHash = await this.global.contract.methods.buyPremium(userData.address).encodeABI();

        let state = await this.global.transaction.send(secretKey.value, {
          to: this.global.constractAddress,
          data: methodsHash,
          value: period.value*pricePremium.value,
          gasPrice: 100000000000,
          gas: 3714100,
        }, userData.id, 'EPay', 'Buy premium');

        resolove({
          status: state.status,
          errors: state.errors,
        });
      }
      else {
        resolove({
          status: false,
          errors: errors,
        })
      }
    
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = premium;