const MODULE_NAME = 'MODEL - isCorrectTotal';
const NUM_DAYS_PREMIUM = require('../../data/numOptiunsOfDaysForPremium');

function isCorrectTotal(totalSrc) {
  return new Promise(async (resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if(totalSrc == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'undefinedTotal')); 
      }
      else if(totalSrc == "") {
        isValid = false;
        errors.push(this.global.messages.getError('pay', 'undefinedTotal')); 
      }
      else {
        let total = await this.global.common.isUInt(totalSrc);
        if(!total.isValid) {
          isValid = false;
          errors.push(this.global.messages.getError('pay', 'notCorrectTotal')); 
        }

        resolove({
          isValid: isValid,
          value: total.value,
          errors: errors
        })
      }

      resolove({
        isValid: isValid,
        value: 0,
        errors: errors
      })

      
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectTotal;