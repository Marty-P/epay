function Messages(language){
  this.language = language;
}

Messages.prototype.getError = require('./getError');
Messages.prototype.getInfo = require('./getInfo');

module.exports = Messages;