const ACCOUNT = require('./account');
const COMMON = require('./common');
const CONSTRUCTOR = require('./constructor');
const MESSAGES = require('./messages');
const PLATFORM = require('./platform');
const WALLET = require('./wallet');
const PAY = require('./pay');
const TRANSACTION = require('./transaction');
const JSON_SMART_CONTRACT = require('../data/smartContracts/epayPremium');

function Model(core, session){
    this.web3 = core.web3;
    this.db = core.database;
    this.emailSender = core.emailSender;
    this.address = core.address;
    this.session = session;
    this.language = 'ru';
    this.constractAddress =  JSON_SMART_CONTRACT.address;
    this.constractInterface = JSON_SMART_CONTRACT.abi;
    this.contract = new this.web3.eth.Contract(this.constractInterface, this.constractAddress);
    
    this.account = new ACCOUNT(this);
    this.common = new COMMON(this);
    this.constructor = new CONSTRUCTOR(this);
    this.messages = new MESSAGES(this.language);
    this.platform = new PLATFORM(this);
    this.wallet = new WALLET(this);   
    this.pay = new PAY(this);
    this.transaction = new TRANSACTION(this);
}

module.exports = Model;