const MODULE_NAME = "MODEL getTokenOfNewPlatform";

function getTokenOfNewPlatform(title) {
  return new Promise(async (resolove, reject) => {
    try{
      let loop = true;
      
      do {
        let id = this.global.common.sha256([this.global.session.idUser, 
                                            new Date().getTime(), title].join());
        try {                                
          let result = await this.global.db.query('SELECT id FROM platforms WHERE id=?', [id])
          if(result.length == 0) {
            loop = false;
            resolove(id);
          }
        }
        catch (error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: SELECT id FROM platforms'
          });
        }
      } while(loop);

    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = getTokenOfNewPlatform;