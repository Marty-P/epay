const MODULE_NAME = "MODEL getOwnerFromPaymentPage";

function getOwnerFromPaymentPage(idPaymentPage) {
  return new Promise(async (resolove, reject) => {
    try {
      let result = await this.global.db.query('SELECT owner FROM payments_pages LEFT JOIN ' + 
      'platforms ON payments_pages.id_platform = platforms.id ' +
       'WHERE payments_pages.id_payment_page = ?', [idPaymentPage]);
      
      if(result.lenght == 0)
        resolove({
          status: false,
          value: 0
        })
      else
        resolove({
          status: true,
          value: result[0].owner
        })

    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = getOwnerFromPaymentPage;