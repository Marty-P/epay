const MODULE_NAME = 'MODEL getListOfStaticIntegrations';

function getListOfStaticIntegrations(idPlatform) {
  return new Promise((resolove, reject) => {
    try{
     
      this.global.db.query('SELECT id_payment_page, name FROM payments_pages WHERE id_platform=? AND deleted=? AND activated=?', [idPlatform, false, true])
      .then((result, fields) => {
        let listOfStaticIntegrations = []

        for(var i = 0; i < result.length; i++) {
          listOfStaticIntegrations.push([result[i]['id_payment_page'], result[i]['name']]);
        }
        
        resolove(listOfStaticIntegrations);
      })

      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: SELECT id_payment_page FROM payments_pages'
        });
      });
    }
    catch(error){

      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });

    }
  });
}

module.exports = getListOfStaticIntegrations;