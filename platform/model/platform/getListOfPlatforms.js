const MODULE_NAME = 'MODEL getListOfPlatforms';

function getListOfPlatforms(id) {
  return new Promise((resolove, reject) => {
    try{
      let idUser = id || this.global.session.idUser

      this.global.db.query('SELECT id, title FROM platforms WHERE owner=?', [idUser])

      .then((result, fields) => {
        let platforms = []

        for(var i = 0; i < result.length; i++) {
          platforms.push([result[i]['id'], this.global.common.getUnescapeStr(result[i]['title'])]);
        }

        resolove(platforms);
      })

      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: SELECT title FROM platforms'
        });
      });
    }
    catch(error){

      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });

    }
  });
}

module.exports = getListOfPlatforms;