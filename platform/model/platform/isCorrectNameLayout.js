const MODULE_NAME = 'MODEL isCorrectNameLayout';

function isCorrectNameLayout(name) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];
      
      if(name == undefined ) {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedLayoutName'));
      }

      else if(name == '') {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedLayoutName'));
      }

      else{
        
        name =  name.trim().replace(/\s+/g,' ');
        let checkName =  name.match(/[a-zA-Zа-яА-Я0-9]{1}[a-zA-Zа-яА-Я0-9\s]{1,29}$/);

        if(checkName == null) {
          isValid = false;
          errors = errors.concat(this.global.messages.getError('platform', 'notCorrectLayoutName'));
        }

        else if (checkName[0] != checkName.input) {
          isValid = false;
          errors = errors.concat(this.global.messages.getError('platform', 'notCorrectLayoutName'));   
        }

        else{
          name = this.global.common.getEscapeStr(name);
          
          if(name.length >= 30) {
            isValid = false;
            errors.push(this.global.messages.getError('platform', 'notCorrectLayoutName'));
          }
        }
      }

      resolove({
        isValid: isValid,
        value: name,
        errors: errors
      });
    
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = isCorrectNameLayout;