const MODULE_NAME = "MODEL getTokenOfNewPaymentPage";

function getTokenOfNewPaymentPage() {
  return new Promise(async (resolove, reject) => {
    try{
      let loop = true;
      
      do {
        let id = this.global.common.sha256([String(Math.random() + Math.random()), 
                                            new Date().getTime()].join());
                                            
        await this.global.db.query('SELECT id_payment_page FROM payments_pages WHERE id_payment_page=?', [id])
        .then((result, fields)=>{
          if(result.length == 0) {
            loop = false;
            resolove(id);
          }
        })
        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: SELECT id FROM platforms'
          });
        });
      } while(loop);

    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = getTokenOfNewPaymentPage;