const MODULE_NAME = 'MODEL getDataOfStaticIntegration';

function getDataOfStaticIntegration(id) {
  return new Promise(async (resolove, reject) => {
    try{
      let paymentsPages = await this.global.db.query('SELECT * FROM payments_pages WHERE id_payment_page = ? ' +
      'AND deleted = ? AND activated = ?', [id, false, true]);
      
      if(paymentsPages.length != 0) {
        let platform = await this.getDataOfPlatform(paymentsPages[0].id_platform);
        
        let address = await this.getWalletOfPlatform(paymentsPages[0].id_platform);
        
        let settings = await this.global.db.query('SELECT title_pp, platform_pp, address_pp, ' +
         'description_pp, title FROM payments_settings LEFT JOIN payments_options ON ' +
         'payments_settings.payment_types_pp = payments_options.id WHERE payments_settings.id_pp = ?;', [id]);

        let values = [];

        if( settings[0].title != 'custom' ) {
          let dbValues = await this.global.db.query('SELECT wei FROM  payments_values WHERE id_payment_page = ?', [id]);
          if(dbValues.length == 1)
            values = dbValues[0].wei;
          else {
            for(let i in dbValues) {
              values.push(dbValues[i].wei)
            }
          }
        }      

        resolove({
          platfomTitle: this.global.common.getUnescapeStr(platform.title),
          walletAddress: address,
          paymentsTitle: this.global.common.getUnescapeStr(paymentsPages[0].title),
          paymentsDiscription: this.global.common.getUnescapeStr(paymentsPages[0].discription),
          name: this.global.common.getUnescapeStr(paymentsPages[0].name),
          settings: {
            title: settings[0].title_pp,
            platform: settings[0].platform_pp,
            address: settings[0].address_pp,
            discription: settings[0].description_pp,
            typePayment: settings[0].title,
          },
          values: values 
        })
      }
      else
        resolove(null)

    }
    catch(error){

      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });

    }
  });
}

module.exports = getDataOfStaticIntegration;