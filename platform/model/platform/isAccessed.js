const MODULE_NAME = 'MODEL isAccessed';

function isAccessed(id){
  return new Promise(async (resolove, reject) => {
    let userId = await this.global.account.getIdFromSession();
    this.global.db.query('SELECT owner FROM platforms WHERE id=?', [id])
    .then((result, fields) => {
      if(result.length == 0)
        resolove(false);
      else
        if(result[0]['owner'] == userId)
          resolove(true);
        else
          resolove(false);
    })
    .catch((error) => {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'db query: SELECT * FROM platforms'
      });
    });
  });
}

module.exports = isAccessed;