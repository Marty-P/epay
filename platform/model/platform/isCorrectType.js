const MODULE_NAME = 'MODEL isCorrectType';

function isCorrectType(type) {
  return new Promise((resolove, reject) => {
    try{
      let isValid = true;
      let value = '';
      let errors = [];

      if(type == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedType'));
      }

      else{
        this.global.db.query('SELECT id FROM types_of_platforms WHERE title=?', [type])

        .then((result, fields) => {
          if(result.length == 0) {
            isValid = false;
            errors.push(this.global.messages.getError('platform', 'notCorrectType'));
          }
          else {
            value = result[0]['id'];
          }
          
          resolove({
            isValid: isValid,
            value: value,
            errors: errors
          });
        })
        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: SELECT id FROM types_of_platforms'
          });
        });
      }
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });  
}

module.exports = isCorrectType;