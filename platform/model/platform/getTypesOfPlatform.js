const MODULE_NAME = "MODEL getTypesOfPlatforms";

function getTypesOfPlatforms(){
  return new Promise((resolove, reject) => {
    try{
      this.global.db.query("SELECT title FROM types_of_platforms ORDER BY id", [])
      .then((result, fields)=>{
        types = []

        for(var i = 0; i < result.length; i++)
          types.push(result[i]['title'])

        resolove(types);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: "db query: SELECT title FROM types_of_platforms"
        });
      });
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "global try"
      });
    }
  });
}

module.exports = getTypesOfPlatforms;