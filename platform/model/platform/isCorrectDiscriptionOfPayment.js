const MODULE_NAME = 'MODEL isCorrectDiscriptionOfPayment';

function isCorrectDiscriptionOfPayment(discription){
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];
      
      if(discription == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedDiscription'));
       
      }

      else if (discription == '') {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedDiscription'));
      }

      else {
        discription = discription.trim().replace(/\s+/g,' ');
        let checkDiscription= discription.match(/[a-zA-Zа-яА-Я0-9\s\-\!\#\$\%\*\(\)\.\,]{1,1000}$/);
      
        if(checkDiscription == null) {
          isValid = false;
          errors.push(this.global.messages.getError('platform', 'notCorrectDiscriptions'));
          errors.push(this.global.messages.getError('platform', 'smallLimitSymbols'));
        }

        else if (checkDiscription[0] != checkDiscription.input) {
          isValid = false;
          errors.push(this.global.messages.getError('platform', 'notCorrectDiscriptions'));  
          errors.push(this.global.messages.getError('platform', 'bigLimitSymbols'));
        }

        else
          discription = this.global.common.getEscapeStr(discription);
          if(discription.length >= 1000)
            errors.push(this.global.messages.getError('platform', 'smallLimitSymbols'));
      }

      resolove({
        isValid: isValid,
        value: discription,
        errors: errors
      });
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = isCorrectDiscriptionOfPayment;

