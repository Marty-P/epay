const MODULE_NAME = "MODEL setWalletOfPlatform";

function setWalletOfPlatform(idSrc, addressSrc){
  return new Promise(async (resolove, reject) => {
    try{
      let errors = [];
    
      let address = await this.global.wallet.isCorrectAddress(addressSrc);
      
      if(address.isValid) {
        try{
          //получаем id зарегистрированного адреса кошелька
          let idOfAddress = await this.global.wallet.getIdByAddress(address.value);

          if(idOfAddress == 1) {
            //если не существует записи то делаем
            let insertedAddress = await this.global.wallet.insertAddress(address.value);

            if(insertedAddress)
              //получаем id зарегистрированного адреса кошелька
              idOfAddress = await this.global.wallet.getIdByAddress(address.value);
            else {
              errors.push(this.global.messages.getError('global', 'unknow'));

              resolove({
                status: false,
                errors: errors
              });
            }
          }
          
          //устанавливаем id адреса кошелька в платформу
          try {
            
            await this.global.db.query('UPDATE platforms SET wallet=? WHERE id=?', [idOfAddress, idSrc]);

            let getWalletOfPlatform = await this.getWalletOfPlatform(idSrc);

            if(getWalletOfPlatform == '') {
              errors.push(this.global.messages.getError('global', 'unknow'));
            }
            else if(getWalletOfPlatform != addressSrc) {
              errors.push(this.global.messages.getError('global', 'unknow'));
            }

            resolove({
              status: errors.length == 0,
              errors: errors
            });

          }
          catch(erorr) {
            reject({
              error: error,
              module: MODULE_NAME,
              comment: 'UPDATE platform SET wallet'
            });
          }

          
        }
        catch(error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'address.isValid'
          });
        }
      }
      else {
        errors = errors.concat(address.errors);

        resolove({
          status: false,
          errors: errors
        });
      }

    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "global try"
      });
    }
  });
}

module.exports = setWalletOfPlatform;