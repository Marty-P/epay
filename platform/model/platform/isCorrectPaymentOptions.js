const MODULE_NAME = 'MODEL isCorrectPaymentOptions';

function isCorrectPaymentOptions(paymentsOptions) {
  return new Promise(async (resolove, reject) => {
    try{
      isValid = true;
      errors = [];

      let payment = {
        type: '',
        value: 0
      } 

      switch(paymentsOptions.type) {
        case 'fix':
          payment.type = 'fix';
          if(paymentsOptions.fix != undefined) {
            if((await this.global.common.isUInt(paymentsOptions.fix)).isValid)
              payment.value = [parseInt(paymentsOptions.fix)];
            else {
              isValid = false;
              payment.value = 0;
              errors.push(this.global.messages.getError('platform', 'notCorrectPrice'));
            }
          }
          else {
            isValid = false;
            errors.push(this.global.messages.getError('platform', 'undefinedPrice'));
          }

          break;
        case 'options':
          payment.type = 'options';
          if(paymentsOptions.numOptions != undefined) {

            if((await this.global.common.isUInt(paymentsOptions.numOptions)).isValid) {
              
              payment.value = [];
              
              for(let i = 1; i <= parseInt(paymentsOptions.numOptions); i++ ) {
                
                if(paymentsOptions.opts['opt' + i.toString()] != undefined)

                  if((await this.global.common.isUInt(paymentsOptions.opts['opt' + i.toString()])).isValid) 
                    payment.value.push( parseInt(paymentsOptions.opts['opt' + i.toString()]));

                  else {
                    isValid = false;
                    errors.push(this.global.messages.getError('platform', 'notCorrectPrice'));
                    break;
                  }
                  
                else {
                  isValid = false;
                  errors.push(this.global.messages.getError('platform', 'undefinedPrice'));
                  break;
                }
              }
            }
            else 
              errors.push(this.global.messages.getError('global', 'unknow'));
          }
          else 
            errors.push(this.global.messages.getError('global', 'unknow'));
          
          break;

        case 'custom':
          payment.type = 'custom';
          break;

        default:
          errors.push(this.global.messages.getError('platform', 'notCorrectTypePyment'));
      }

      resolove({
        isValid: isValid,
        out: payment,
        errors: errors
      })
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });  
}

module.exports = isCorrectPaymentOptions;