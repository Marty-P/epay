const MODULE_NAME = 'MODEL isRegistredTitle';

function isFreeTitle(title){
  return new Promise((resolove, reject) => {
    this.global.db.query('SELECT * FROM platforms WHERE title=?', [title])
    .then((result, fields) => {
      resolove(result.length == 0)
    })
    .catch((error) => {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'db query: SELECT * FROM platforms'
      });
    });
  });
}

module.exports = isFreeTitle;