const MODULE_NAME = 'MODEL addPlatform';

function add(titleSrc, discriptionSrc, typeSrc) {
  return new Promise(async (resolove, reject) => {
    try{
      let errors = [];
      //проверка названия
      let title = await this.isCorrectNewTitle(titleSrc);
      
      if(title.isValid) {
        try{
          let isFreeTitle = await this.isFreeTitle(title.value);

          if(!isFreeTitle)
            errors.push(this.global.messages.getError('platform', 'isRegedTitle'));

        }
        catch(error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'title.isValid'
          });
        }
      }
      else {
        errors = errors.concat(title.errors);
      }
      
      //проверка описания
      let discription = await this.isCorrectNewDiscription(discriptionSrc);
      errors = errors.concat(discription.errors);
      
      //проверка типа
      let type = await this.isCorrectType(typeSrc);
      errors = errors.concat(type.errors);
      
      if(errors.length == 0) {
        
        let id = await this.getTokenOfNewPlatform(title.value);
        this.global.db.query('INSERT INTO platforms (id, owner, title,' + 
        'discription, type) VALUES (?, ?, ?, ?, ?);', [id, 
          this.global.session.idUser, title.value, discription.value, type.value])

        .then((result, fields) => {
          
          resolove({
            status: true,
            errors: errors
          });
        })

        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: INSERT INTO platform'
          });
        });
      }
      else {
        resolove({
          status: false,
          errors: errors
        });
      }

    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = add;