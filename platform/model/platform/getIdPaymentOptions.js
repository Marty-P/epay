const MODULE_NAME = 'MODEL getIdPaymentOptions';

function getIdPaymentOptions(title) {
  return new Promise((resolove, reject) => {
    try {
      this.global.db.query('SELECT id FROM payments_options WHERE title=?;', [title])
      .then(async (result, fields) => {
        if(result == 0) {
          let id = await this.getIdPaymentOptions('custom');
          resolove(id);
        }
        else
          resolove(result[0]['id'])
      })

      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: SELECT title FROM platforms'
        });
      });
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });

    }
  });
}

module.exports = getIdPaymentOptions;