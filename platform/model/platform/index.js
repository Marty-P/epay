function Platform(global) {
  this.global = global;
}

Platform.prototype.add = require('./add');
Platform.prototype.newStaticIntegration = require('./newStaticIntegration');

Platform.prototype.isAccessed = require('./isAccessed');
Platform.prototype.isCorrectDiscriptionOfPayment = require('./isCorrectDiscriptionOfPayment');
Platform.prototype.isCorrectNewDiscription = require('./isCorrectNewDiscription');
Platform.prototype.isCorrectNewTitle = require('./isCorrectNewTitle');
Platform.prototype.isCorrectTitleOfPayment = require('./isCorrectTitleOfPayment');
Platform.prototype.isCorrectPaymentOptions = require('./isCorrectPaymentOptions');
Platform.prototype.isCorrectNameLayout = require('./isCorrectNameLayout');
Platform.prototype.isCorrectType = require('./isCorrectType');
Platform.prototype.isFreeTitle = require('./isFreeTitle');

Platform.prototype.getDataOfPlatform = require('./getDataOfPlatform');
Platform.prototype.getDataOfStaticIntegration = require('./getDataOfStaticIntegration');
Platform.prototype.getListOfPlatforms = require('./getListOfPlatforms');
Platform.prototype.getListOfStaticIntegrations = require('./getListOfStaticIntegrations');
Platform.prototype.getTokenOfNewPlatform = require('./getTokenOfNewPlatform');
Platform.prototype.getTokenOfNewPaymentPage = require('./getTokenOfNewPaymentPage');
Platform.prototype.getTypesOfPlatform = require('./getTypesOfPlatform');
Platform.prototype.getWalletOfPlatform = require('./getWalletOfPlatform');
Platform.prototype.getIdPaymentOptions = require('./getIdPaymentOptions');
Platform.prototype.getOwnerFromPaymentPage = require('./getOwnerFromPaymentPage');

Platform.prototype.setWallet = require('./setWallet');


module.exports = Platform;