const MODULE_NAME = 'MODEL newStaticIntegration';

function newStaticIntegration(data, idPlatform) {
  return new Promise(async (resolove, reject) => {
    try {
      let errors = [];

      let flags = {
        title: false,
        platform: false,
        address: false,
        discription: false,
        paymentOptions: '',
      };

      let paymentPage = {
        title: 'NULL',
        discription: 'NULL',
      }

      if(data.flagTitle != undefined) {
        flags.title = true;
        let title = await this.isCorrectTitleOfPayment(data.title);
        errors = errors.concat(title.errors);
        if(title.isValid)
          paymentPage.title = title.value;
      }

      let name = await this.isCorrectNameLayout(data.name)
      errors = errors.concat(name.errors);

      if(data.flagPlatform != undefined)
        flags.platform = true;

      if(data.flagAddress != undefined)
        flags.address = true;

      if(data.flagDiscription != undefined) {
        flags.discription = true;
        let discription = await this.isCorrectDiscriptionOfPayment(data.discription);
        errors = errors.concat(discription.errors);
        if(discription.isValid)
          paymentPage.discription = discription.value;
      }

      let paymentsOptions = await this.isCorrectPaymentOptions({
        type: data.options,
        fix: data.fix,
        numOptions: data.numOpts,
        opts: {
          opt1: data.opt1,
          opt2: data.opt2,
          opt3: data.opt3,
          opt4: data.opt4,
          opt5: data.opt5,
        }
      })

      errors = errors.concat(paymentsOptions.errors);

      if(errors.length == 0) {
        flags.paymentOptions = paymentsOptions.out.type;

        let idPaymentPage = await this.getTokenOfNewPaymentPage();
        let idPaymentOptions = await this.getIdPaymentOptions(flags.paymentOptions)

        try {
          

          await this.global.db.query('INSERT INTO payments_pages (id_payment_page, id_platform, title, discription, name) VALUES (?,?,?,?,?);',
                                    [idPaymentPage, idPlatform, paymentPage.title, paymentPage.discription, name.value])
        
          await this.global.db.query('INSERT INTO payments_settings (id_pp, title_pp, platform_pp, address_pp, ' + 
                                      'description_pp, payment_types_pp) VALUES (?,?,?,?,?,?);',
                                    [idPaymentPage, flags.title, flags.platform, flags.address, flags.discription, idPaymentOptions]);
          
          if(flags.paymentOptions != 'custom') {
            for(let i in paymentsOptions.out.value) {
              await this.global.db.query('INSERT INTO payments_values (id_payment_page, wei) VALUES (?,?);',
                                        [idPaymentPage, paymentsOptions.out.value[i]])
            }
          }

          await this.global.db.query('UPDATE payments_pages SET activated=true',[]);

          resolove({
            status: true,
            errors: errors
          })
      
        }
        catch(error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'global try'
          });
        };

      }
      else 
        resolove({
          status: false,
          errors: errors
        })
      
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = newStaticIntegration;