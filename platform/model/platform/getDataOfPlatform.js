const MODULE_NAME = "MODEL getDataOfPlatform";

function getDataOfPlatform(id){
  return new Promise((resolove, reject) => {
    try{
      this.global.db.query("SELECT * FROM platforms WHERE id = ?;", [id])
      .then((result, fields)=>{
        if(result.length == 0)
          resolove({})
        else
          resolove(result[0]);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: "SELECT * FROM platforms"
        });
      });
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "global try"
      });
    }
  });
}

module.exports = getDataOfPlatform;