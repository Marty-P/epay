const MODULE_NAME = 'MODEL isCorrectTitleOfPayment';

function isCorrectTitleOfPayment(title) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if(title == undefined ) {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedTitleOfPayment'));
      }

      else if(title == '') {
        isValid = false;
        errors.push(this.global.messages.getError('platform', 'undefinedTitleOfPayment'));
      }

      else{
        title =  title.trim().replace(/\s+/g,' ');
        let checkTitle =  title.match(/[a-zA-Zа-яА-Я0-9]{1}[a-zA-Zа-яА-Я0-9\s\-\_\!\#\$\%\&\*\(\)\|\.\,]{1,39}$/);
        
        if(checkTitle == null) {
          isValid = false;
          errors = errors.concat(this.global.messages.getError('platform', 'notCorrectTitle'));
        }

        else if (checkTitle[0] != checkTitle.input) {
          isValid = false;
          errors = errors.concat(this.global.messages.getError('platform', 'notCorrectTitle'));   
        }

        else{
          title = this.global.common.getEscapeStr(title);

          if(title.length >= 1000)
            errors.push(this.global.messages.getError('platform', 'notCorrectTitle'));
        }
      }

      resolove({
        isValid: isValid,
        value: title,
        errors: errors
      });
    
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = isCorrectTitleOfPayment;