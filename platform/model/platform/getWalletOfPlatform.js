const MODULE_NAME = "MODEL getWalletOfPlatform";

function getWalletOfPlatform(id){
  return new Promise((resolove, reject) => {
    try{
      this.global.db.query("SELECT address FROM platforms LEFT JOIN wallets ON " + 
      "platforms.wallet = wallets.id WHERE platforms.id = ?;", [id])
      .then((result, fields)=>{
        if(result.length == 0)
          resolove('')
        else
          resolove(result[0].address);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: "SELECT address FROM platforms LEFT JOIN wallets"
        });
      });
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "global try"
      });
    }
  });
}

module.exports = getWalletOfPlatform;