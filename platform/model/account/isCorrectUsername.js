const MODULE_NAME = 'MODEL isCorrectUsername';

function isCorrectUsername(username) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if(username == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedUsername'));
      }

      else if(username == '') {
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedUsername'));
      }

      else{
        username = username.trim().replace(/\s+/g,' ');
        checkUsername = username.match(/[a-zA-Z]{1}\w{2,31}$/)

        if(checkUsername == null)
          isValid = false;
          
        else if(checkUsername[0] != checkUsername.input)
          isValid = false;

        if(!isValid)
          errors = errors.concat(this.global.messages.getError('account', 'notCorrectUsername'));
        else
          username = this.global.common.getEscapeStr(username);
          if(username.length >= 31) {
            isValid = false;
            errors.push(this.global.messages.getError('account', 'notCorrectUsername'));
          }
      }

      resolove({
        isValid: isValid,
        value:  username,
        errors: errors
      })
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  })
}

module.exports = isCorrectUsername;