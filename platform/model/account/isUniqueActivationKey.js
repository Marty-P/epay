const MODULE_NAME = 'MODEL isUniqueActivationKey';

function isUniqueActivationKey(key){
  return new Promise((resolove, reject) => {
    this.global.db.query('SELECT * FROM activation_keys WHERE activation_key=?', [key])
    .then((result, fields) => {
      resolove(result.length == 0)
    })
    .catch((error) => {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'db query: SELECT * FROM activation_keys'
      });
    })
  });
}

module.exports = isUniqueActivationKey;