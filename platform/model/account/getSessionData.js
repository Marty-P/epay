const MODULE_NAME = 'MODEL getSessionData';

function getSessionData(){
  return new Promise(async (resolove, reject) => {
    try {
      if(await this.isAuthorized())
        resolove({
          id: this.global.session.idUser,
          username: this.global.session.username,
          address: this.global.session.address,
          role: this.global.session.role
        })
      else
        resolove(0)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  });
}

module.exports = getSessionData;