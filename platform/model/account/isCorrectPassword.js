const MODULE_NAME = 'MODEL account - isCorrectPassword';

function isCorrectPassword(password) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if(password == undefined){
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedPassword')); 
      }
      else if(password == ''){
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedPassword')); 
      }
      else{
        /* непонятно как совместить модуль авторизации и регистрации
        if(data.password.length < 8){
          isValid = false;
          errors.push(MSG[data.lang].profile.errors.notCorrectPassword);
        }
        */
        password = this.global.common.sha256(password);
      }

      resolove({
        isValid: isValid,
        value: password,
        errors: errors
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectPassword;