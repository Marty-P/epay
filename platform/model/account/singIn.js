const MODULE_NAME = 'MODEL singIn';

function singIn(emailSrc, passwordSrc) {
  return new Promise(async (resolove, reject) => {
    try{
      let errors = [];

      let email = await this.isCorrectEmail(emailSrc);
      errors = errors.concat(email.errors);
      
      let password = await this.isCorrectPassword(passwordSrc);
      errors = errors.concat(password.errors);
      
      if(password.isValid && email.isValid) {
        this.global.db.query('SELECT id, role, username, address FROM users WHERE ' +
                        'email=? AND password=?;', [email.value, password.value])

        .then((result, fields) => {
          
          if(result.length == 1) {
            this.global.session.idUser = result[0]['id'];
            this.global.session.role = result[0]['role'];
            this.global.session.username = result[0]['username'];
            this.global.session.address = result[0]['address'];

            resolove({
              status: true,
              errors: errors
            });

          }
          else{
            errors = errors.concat(this.global.messages.getError('account', 'incorrectEmailOrPassword'));
            resolove({
              status: false,
              errors: errors
            });
          }
        })

        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: SELECT id, role FROM users'
          });
        });

      }
      else
        resolove({
          status: false,
          errors: errors
        }); 

    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });  
    }
  });
}

module.exports = singIn;