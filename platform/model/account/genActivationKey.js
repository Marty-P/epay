const MODULE_NAME = 'MODEL genActivationKey';

function genActivationKey(username) {
  return new Promise(async (resolove, reject) => {
    try {
      let loop = true;
      
      do {
        let key = this.global.common.sha256(String(Math.random() + Math.random()));
        
        let id = await this.getIdByUsername(username);
        
        if(id != 0) {
          let isUniqueKey = await this.isUniqueActivationKey(key);

          if(isUniqueKey){
            await this.global.db.query('INSERT INTO activation_keys ' + 
                          '(id_user, activation_key) VALUES (?,?);', [id, key])
            
            .then((result, fields) => {
              loop = false;
              resolove({
                key: key,
                errors: []
              });
            })
            
            .catch((error) => {
              reject({
                error: error,
                module: MODULE_NAME,
                comment: 'db query: INSERT INTO activation_keys'
              });
            });
          }
        }
        else {
          loop = false;
          resolove({
            key: '',
            errors: [this.global.messages.getError('account', 'logicalInconsistencyActivationKey')]
          });
        }
      } while(loop);

    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  });
}

module.exports = genActivationKey;