const MODULE_NAME = 'MODEL getIdByKey';

function getIdByKey(key){
  return new Promise((resolove, reject) => {
    this.global.db.query('SELECT id_user FROM activation_keys WHERE ' + 
                      'activation_key=? AND used=?', [key, false])
    .then((result, fields) => {
      if(result.length != 0)
        resolove(result[0].id_user)
      else
        resolove(0)
    })
    .catch((error) => {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'db query: SELECT id_user FROM activation_keys'
      });
    });
  });
}

module.exports = getIdByKey;