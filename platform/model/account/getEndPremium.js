const MODULE_NAME = 'MODEL getEndPremium';

function getEndPremium(address) {
  return new Promise(async (resolove, reject) => {
    try {
      resolove(await this.global.contract.methods.getEndPremium(address).call());
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      });
    }
  })
};

module.exports = getEndPremium;