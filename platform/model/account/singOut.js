const MODULE_NAME = 'MODEL singOut';

function singOut(){
  this.global.session.destroy();
}

module.exports = singOut;