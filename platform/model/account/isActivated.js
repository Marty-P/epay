const MODULE_NAME = 'MODEL isAuthorized';

function isActivated(id) {
  return new Promise((resolove, reject) => {
    this.global.db.query("SELECT activated FROM users WHERE id=?", [id])

    .then((result, fields) => {
      if(result.length != 0)
        resolove(result[0].activated == 1);
      else
        resolove('0');
    })

    .catch((error) => {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "db query: SELECT activated FROM users"
      });
    })
  });
}

module.exports = isActivated;