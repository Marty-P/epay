function Account(global){
  this.global = global;
}

Account.prototype.singIn = require('./singIn');
Account.prototype.singOut = require('./singOut');
Account.prototype.singUp = require('./singUp');
Account.prototype.genActivationKey = require('./genActivationKey');
Account.prototype.activate = require('./activate');

Account.prototype.isActivated = require('./isActivated');
Account.prototype.isAuthorized = require('./isAuthorized');
Account.prototype.isCorrectEmail = require('./isCorrectEmail');
Account.prototype.isCorrectUsername = require('./isCorrectUsername');
Account.prototype.isCorrectPassword = require('./isCorrectPassword');
Account.prototype.isFreeEmail = require('./isFreeEmail');
Account.prototype.isFreeUsername = require('./isFreeUsername');
Account.prototype.isPremium = require('./isPremium');
Account.prototype.isUniqueActivationKey = require('./isUniqueActivationKey');

Account.prototype.getEndPremium = require('./getEndPremium');
Account.prototype.getIdByKey = require('./getIdByKey');
Account.prototype.getIdByUsername = require('./getIdByUsername');
Account.prototype.getIdFromSession = require('./getIdFromSession');
Account.prototype.getSessionData = require('./getSessionData');

module.exports = Account;