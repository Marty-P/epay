const MODULE_NAME = 'MODEL getIdByUsername';

function getIdByUsername(username){
  return new Promise((resolove, reject) => {
    this.global.db.query('SELECT id FROM EPay.users WHERE username=?', [username])
    .then((result, fields) => {
      if(result.length != 0)
        resolove(result[0].id)
      else
        resolove(0)
    })
    .catch((error) => {
      
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'db query: SELECT id FROM users'
      });
    })
  });
}

module.exports = getIdByUsername;