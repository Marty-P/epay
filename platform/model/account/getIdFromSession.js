const MODULE_NAME = 'MODEL getIdFromSession';

function getIdFromSession(){
  return new Promise(async (resolove, reject) => {
    try {
      if(await this.isAuthorized())
        resolove(this.global.session.idUser)
      else
        resolove(0)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  });
}

module.exports = getIdFromSession;