const MODULE_NAME = 'MODEL isAuthorized';

function isAuthorized() {
  return new Promise((resolove, reject) => {
    try{
      if(this.global.session.idUser)
        resolove(true);
      else 
        resolove(false);
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'Check session'
      });
    }

  })
}

module.exports = isAuthorized;