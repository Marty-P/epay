const MODULE_NAME = 'MODEL isFreeEmail';

function isFreeEmail(email){
  return new Promise((resolove, reject) => {
    this.global.db.query('SELECT * FROM users WHERE email=?;', [email])
    .then((result, fields) => {
      resolove(result.length == 0)
    })
    .catch((error) => {
      reject({error: error, module: MODULE_NAME, comment: 'db query: SELECT * FROM users'});
    });
  });
}

module.exports = isFreeEmail;