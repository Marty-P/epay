const MODULE_NAME = "MODEL activateProfile";

function activate(key){
  return new Promise(async (resolove, reject) => {
    try{
      let idUser = await this.global.account.getIdByKey(key);
      
      if(idUser != 0){
        this.global.db.query("UPDATE activation_keys SET used=true WHERE activation_key=?", [key])
        .then((result, fields)=>{

          this.global.db.query("UPDATE users SET activated=1 WHERE id=?", [idUser])
          .then(async (result, fields) => {

            resolove({
                status: await this.isActivated(idUser),
                errors: []
              });

          })
          .catch((error) => {
            reject({
              error: error,
              module: MODULE_NAME,
              comment: "db query: UPDATE users"
            });
          });

        })
        .catch((error) => {
          reject({
            error: error, module:
            MODULE_NAME,
            comment: "db query: UPDATE activation_keys"
          })
        })
      }
      else{
        resolove({
          status: false,
          errors: [this.global.messages.getError('account', 'incorrectActivationKey')]
        });
      }
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: "global try"
      });
    }

  });
}

module.exports = activate;