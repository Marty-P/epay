const MODULE_NAME = 'MODEL singUp';

function singUp(usernameSrc, passwordSrc, emailSrc) {
  return new Promise(async (resolove, reject) => {
    try{
      let errors = []

      //проверка имени пользователя
      let username = await this.isCorrectUsername(usernameSrc);
      
      if(username.isValid) {
        try{
          if(!(await this.isFreeUsername(username.value)))
            errors.push(this.global.messages.getError('account', 'isNotFreeUsername'));
        }
        catch(error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'username.isValid'
          });
        }
      }
      else
        errors = errors.concat(username.errors)

      //проверка электронной почты
      
      let email = await this.isCorrectEmail(emailSrc);

      if(email.isValid) {
        try{
          if(!(await this.isFreeEmail(email.value)))
            errors.push(this.global.messages.getError('account', 'isNotFreeEmail'));
        }
        catch(error) {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'email.isValid'
          });
        }
      }
      else
        errors = errors.concat(email.errors);
      
      //проверка пароля
      let password = await this.isCorrectPassword(passwordSrc);

      if(!password.isValid)
        errors = errors.concat(password.errors);

      //создание адреса кошелька
      let passForAddress =  this.global.common.sha256(String(Math.random() + Math.random()));
      let address = (await this.global.wallet.create(passForAddress, passForAddress)).address;
      
      if(errors.length == 0) {
        this.global.db.query('INSERT INTO users (address, username, email, password) VALUES (?,?,?,?);', 
                          [address, username.value, email.value, password.value])
        .then(async (result, fields) => {
          try{
            let genKey = await this.genActivationKey(username.value);
            
            if(genKey.key != '') {

              this.global.emailSender.send(this.global.common.getUnescapeStr(email.value),
                                       "Registration", this.global.address + '/account/activate?key=' + genKey.key);

              resolove({
                status: true,
                errors: errors
              });

            }
            else {
              errors = errors.concat(genKey.errors);

              resolove({
                status: false,
                errors: errors
              });
            }
          }
          catch(error) {
            reject({
              error: error,
              module: MODULE_NAME,
              comment: 'generate key activation'
            });
          }
        })
        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: INSERT INTO users'
          });
        });
      }
      else {
        resolove({
          status: false,
          errors: errors
        });
      }
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  });
}

module.exports = singUp;