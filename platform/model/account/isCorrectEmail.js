const MODULE_NAME = 'MODEL isCorrectEmail';

function isCorrectEmail(email) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = []; 
      
      if(email == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedEmail')); 
      }
      else if(email == '') {
        isValid = false;
        errors.push(this.global.messages.getError('account', 'undefinedEmail'));
        
      }
      else {
        email = email.trim().replace(/\s+/g,' ');
        //let checkEmail = email.match(/\w{1,}@\w{1,}$/);
        let checkEmail = email.match(/[a-zA-Z0-9\.\-\_]{1,}@[a-zA-Z0-9.]{1,}$/);

        if(checkEmail == null)
          isValid = false;
        else if(checkEmail[0] != checkEmail.input || email.length > 254)
          isValid = false;
        
        if(!isValid)
          errors = errors.concat(this.global.messages.getError('account', 'notCorrectEmail'));
        else {
          email = this.global.common.getEscapeStr(email);
          if(email.length >= 254) {
            isValid = false;
            errors.push(this.global.messages.getError('account', 'notCorrectEmail'));
          }
        }
      }
      
      resolove({
        isValid: isValid,
        value:  email,
        errors: errors
      });
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  });
}

module.exports = isCorrectEmail;