const MODULE_NAME = "CONSTRUCTOR complexBuild";

function simpleBuild(page, subPage, opt) {
  return new Promise(async (resolove, reject) => {
    try {
      let options = opt || {};

      let base = await this.getBasicData();

      let data = {
        page: page,
        subPage: subPage,
        titlePage: options.title || 'EPay',
      };

      Object.assign(data, base);
      let addition = {};

      //генерация данных для макетов
      switch(page) {
        case 'platform':
          addition = await this.additionsBuilders.platform(options);
          data.titlePage = addition.platformTitle;
          break;
        case 'addPlatform':
          addition = await this.additionsBuilders.addPlatform(options);
          break;
        case 'createWallet':    
          addition = await this.additionsBuilders.createWallet(options);
          break;
        case 'setWalletOnPlatform':
          addition = await this.additionsBuilders.setWalletOnPlatform(options);
          data.titlePage = addition.platformTitle;
          break;
        case 'buyPremium':
          addition = await this.additionsBuilders.buyPremium(options);
          break;
        case 'feed':
          addition = await this.additionsBuilders.feed(options);
          break;
        case 'staticIntegration':
          addition = await this.additionsBuilders.staticIntegration(options);
          break;
        case 'newStaticIntegration':
          addition = await this.additionsBuilders.newIntegration(options);
          break;
        case 'pay':
          addition = await this.additionsBuilders.pay(options);
          break;
          
      }

      Object.assign(data, addition);

      if(options.errors != undefined) {
        base.errors.error = true;
        base.errors.messages = options.errors;
      }
        
      resolove(data);
      
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  })

}

module.exports = simpleBuild;