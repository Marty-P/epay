const MODULE_NAME = "CONSTRUCTOR getBasicData";

function getBasicData(){
  return new Promise(async (resolove, reject) => {
    try {
      let data = {
        lang: this.lang,
        server: this.server,
        isAuthorized: await this.global.account.isAuthorized(),
        errors: {
          error: false,
          messages: []
        },
      }

      if(data.isAuthorized) {
        let sessionData = await this.global.account.getSessionData();
        data.id = sessionData.id;
        data.username = sessionData.username;
        data.premium = await this.global.account.isPremium(sessionData.address);
        data.platforms = await this.global.platform.getListOfPlatforms(sessionData.id);
      }

      resolove(data);
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = getBasicData;