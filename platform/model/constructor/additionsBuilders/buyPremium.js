const MODULE_NAME = "ADDITIONS_BUILDERS buyPremium";
const NUM_DAYS_PREMIUM = require('../../../data/numOptiunsOfDaysForPremium');

function buyPremium(options) {
  return new Promise(async (resolove, reject) => {
    try {
      
      let costPremium = await this.global.pay.getPricePremium('ether');
      let unit = costPremium.unit.toUpperCase()
      let listPeriods = [];
      for(let i in NUM_DAYS_PREMIUM){
        listPeriods.push([
          NUM_DAYS_PREMIUM[i].toString() + ' дней',
          costPremium.value*NUM_DAYS_PREMIUM[i],
          unit,
          NUM_DAYS_PREMIUM[i]
        ])
      }

      let addition = {
        listPeriods: listPeriods
      }
      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = buyPremium;