const MODULE_NAME = "ADDITIONS_BUILDERS staticIntegration";

function staticIntegration(options) {
  return new Promise(async (resolove, reject) => {
    try {
     
      let addition = {
        link: this.global.address + '/pay?id=' + options.id,
        framePayLink: this.global.address + '/framePay?id=' + options.id,
      };

      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = staticIntegration;