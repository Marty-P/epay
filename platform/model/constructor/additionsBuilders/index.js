function AdditionsBuilders(global){
  this.global = global;
}

AdditionsBuilders.prototype.platform = require('./platform');
AdditionsBuilders.prototype.addPlatform = require('./addPlatform');
AdditionsBuilders.prototype.createWallet = require('./createWallet');
AdditionsBuilders.prototype.setWalletOnPlatform = require('./setWalletOnPlatform');
AdditionsBuilders.prototype.buyPremium = require('./buyPremium');
AdditionsBuilders.prototype.feed = require('./feed');
AdditionsBuilders.prototype.staticIntegration = require('./staticIntegration');
AdditionsBuilders.prototype.newIntegration = require('./newIntegration');
AdditionsBuilders.prototype.pay = require('./pay');

module.exports = AdditionsBuilders;