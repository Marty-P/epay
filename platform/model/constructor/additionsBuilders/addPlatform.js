const MODULE_NAME = "ADDITIONS_BUILDERS addPlatform";

function addPlatform(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let addition = {
        typesOfPlatforms: await this.global.platform.getTypesOfPlatform(),
      }
      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = addPlatform;