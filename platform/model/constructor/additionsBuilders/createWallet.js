const MODULE_NAME = "ADDITIONS_BUILDERS createWallet";

function createWallet(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let addition = {
        address: options.address || '',
        privateKey: options.privateKey || '',
        keyStone: JSON.stringify(options.keyStone) || '',
        setOnPlatform: options.setOnPlatform || false,
        setedAddressOnPlatform: options.setedAddressOnPlatform || false
      }
      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = createWallet;