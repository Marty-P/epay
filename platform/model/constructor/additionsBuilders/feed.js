const MODULE_NAME = "ADDITIONS_BUILDERS feed";

function setWalletOnPlatform(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let listStateses = await this.global.transaction.getStatuses();

      let listTransactions = await this.global.transaction.getForAccount();
      let list = [];
      for(let i in listTransactions){
        list.push({
          id: listTransactions[i].id,
          whose: listTransactions[i].whose,
          hash: listTransactions[i].hash,
          shortHash: listTransactions[i].hash.substr(0,50) + '.....',
          platform: listTransactions[i].platform,
          type: listTransactions[i].type,
          status: listStateses[listTransactions[i].status - 1].title,
        })
      }
      let addition = {
        listT: list,
      }

      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = setWalletOnPlatform;