const MODULE_NAME = "ADDITIONS_BUILDERS setWalletOnPlatform";

function setWalletOnPlatform(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let dataForSetWallet = await this.global.platform.getDataOfPlatform(options.id);

      let addition = {
        platformId: dataForSetWallet.id,
        platformTitle: dataForSetWallet.title,
      }

      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = setWalletOnPlatform;