const MODULE_NAME = "ADDITIONS_BUILDERS platform";

function platform(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let dataForPlatformPage = await this.global.platform.getDataOfPlatform(options.id);
      let walletOfPlatform = await this.global.platform.getWalletOfPlatform(options.id);
      let listStaticIntegrations = await this.global.platform.getListOfStaticIntegrations(options.id)
      let balance = {
        value: 0,
        unit: 'ether'
      }
      
      if(walletOfPlatform != 'NULL')
        balance = await this.global.wallet.getBalance(walletOfPlatform, 'ether');
  
      let addition = {
        platformId: dataForPlatformPage.id,
        platformTitle: this.global.common.getUnescapeStr(dataForPlatformPage.title),
        platformDiscription: dataForPlatformPage.discription,
        platformType: dataForPlatformPage.type,
        platformWallet: walletOfPlatform,
        platformLocked: dataForPlatformPage.locked,
        platformDeleted: dataForPlatformPage.deleted,
        platformBalance: balance.value,
        platformUnit: balance.unit.toUpperCase(),
        listStaticIntegrations: listStaticIntegrations
      }
      
      resolove(addition)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = platform;