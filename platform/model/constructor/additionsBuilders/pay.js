const MODULE_NAME = "ADDITIONS_BUILDERS pay";

function pay(options) {
  return new Promise(async (resolove, reject) => {
    try {
      let infoAboutPayment = await this.global.platform.getDataOfStaticIntegration(options.id);      
      resolove(infoAboutPayment)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = pay;