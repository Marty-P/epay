const ADDITIONS_BUILDERS = require('./additionsBuilders');

function Constructor(global) {
  this.global = global;
  this.server = global.address;
  this.lang = "ru";

  this.additionsBuilders = new ADDITIONS_BUILDERS(global);
}

Constructor.prototype.message = require('./message');

Constructor.prototype.complexBuild = require('./complexBuild');
Constructor.prototype.getBasicData = require('./getBasicData');
Constructor.prototype.simpleBuild = require('./simpleBuild');


module.exports = Constructor;