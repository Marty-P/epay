const MODULE_NAME = "CONSTRUCTOR message";

function message(type, title, text) {
  return new Promise(async (resolove, reject) => {
    try {
      let data = {
        page: 'message',
        subPage: '',
        titlePage: title,
        messageType: type,
        messageTitle: title,
        messageText: text
      };
    
      let base = await this.getBasicData();
        
      resolove(Object.assign(data, base));
      
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  })

}

module.exports = message;