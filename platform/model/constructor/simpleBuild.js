const MODULE_NAME = "CONSTRUCTOR simpleBuild";

function simpleBuild(opt) {
  return new Promise(async (resolove, reject) => {
    try {
      let options = opt || {};

      let base = await this.getBasicData();

      let data = {
        page: options.page || 'welcome',
        subPage: options.subpage || '',
        titlePage: options.title || 'EPay',
      };

      if((base.isAuthorized) && (data.page == 'welcome')) {
        data.page = 'feed';
        let listTransactions = await this.global.transaction.getForAccount();
        let list = [];
        for(let i in listTransactions){
          list.push([
            listTransactions[i].id,
            listTransactions[i].whose,
            listTransactions[i].hash,
            listTransactions[i].platform,
            listTransactions[i].type,
            listTransactions[i].status,
          ])
        }
        data.listT = list;
      }

      if(options.errors != undefined) {
        base.errors.error = true;
        base.errors.messages = options.errors;
      }
        
      resolove(Object.assign(data, base));
      
    }
    catch(error){
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  })

}

module.exports = simpleBuild;