const querystring = require('querystring');

function Common(global){
  this.global = global;
}

Common.prototype.getEscapeStr = function(str) {
  return querystring.escape(str);
}

Common.prototype.getUnescapeStr = function(str) {
  return querystring.unescape(str);
}


Common.prototype.isUInt = function(str){
  return new Promise((resolove, reject) => {
    try {
      str = str.toString();
      checkStr = str.match(/[0-9]{1,}/);
      if(checkStr == null)
        resolove({
          isValid: false,
          value: 0
        });
      else
        if(checkStr[0] == str)
          resolove({
            isValid: true,
            value: parseInt(str)
          });
        else
          resolove({
            isValid: false,
            value: 0
          });
        
    }
    catch(error) {
      reject({
        error: error,
        module: 'COMMON isUInt ',
        comment: 'global try'
      })
    }
  })
  
}

Common.prototype.sha256 = require('sha256');

module.exports = Common;