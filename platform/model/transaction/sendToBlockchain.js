const MODULE_NAME = 'MODEL sendToBlockChain';

function sendToBlockChain(signedTransaction) {
  return new Promise(async (resolove, reject) => {
    try {
      await this.global.web3.eth.sendSignedTransaction(signedTransaction)
      .once('transactionHash', function(hash){ 
        resolove({
          status: true,
          out: hash
        })
      })
      .on('error', function(error){
        resolove({
          status: false,
          out: error
        })
       });
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = sendToBlockChain;