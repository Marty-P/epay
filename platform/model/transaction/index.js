function Transaction(global) {
  this.global = global;
}

Transaction.prototype.sign = require('./sign');
Transaction.prototype.send = require('./send');
Transaction.prototype.sendToBlockChain = require('./sendToBlockchain');
Transaction.prototype.insertToDataBase = require('./insertToDataBase');
Transaction.prototype.getForAccount = require('./getForAccount');
Transaction.prototype.getStatuses = require('./getStatuses');


module.exports = Transaction;