const MODULE_NAME = 'MODEL singTransaction';

function singTransaction(secretKey, data) {
  return new Promise(async (resolove, reject) => {
    try {
      let signedTransaction = await this.global.web3.eth.accounts.signTransaction(data, secretKey);

      resolove(signedTransaction.rawTransaction)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = singTransaction;