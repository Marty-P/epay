const MODULE_NAME = 'MODEL insertToDataBase';

function insertToDataBase(whose, hash, platform, type) {
  return new Promise((resolove, reject) => {
    try {
      this.global.db.query('INSERT INTO transactions (whose, hash, platform, type, status) VALUES (?,?,?,?,?)',
                        [whose, hash, platform, type, 1])
      .then((result, fields) => {
        
        this.global.db.query('SELECT * FROM transactions WHERE hash=?', [hash])
        .then((result, fields) => {
          resolove(result.lenth != 0);
        })
        .catch((error) => {
          reject({
            error: error,
            module: MODULE_NAME,
            comment: 'db query: SELECT * FROM transactions'
          });
        })
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: INSERT INTO transactions'
        });
      });
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = insertToDataBase;