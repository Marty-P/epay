const MODULE_NAME = 'MODEL send';

function send(privateKey, data, owner, platform, type) {
  return new Promise(async (resolove, reject) => {
    try {
      let dataTX = {};

      if(data.to != undefined)
        dataTX.to = data.to;

      if(data.data != undefined)
        dataTX.data = data.data;
      
      if(data.value != undefined)
        dataTX.value = data.value;

      if(data.gasPrice != undefined)
        dataTX.gasPrice = data.gasPrice;

      if(data.gas != undefined)
        dataTX.gas = data.gas;

      
      
      let signedTransaction = await this.sign(privateKey, dataTX);

      if(signedTransaction != undefined) {
       
        let hashOfTransaction = await this.sendToBlockChain(signedTransaction);

        if(hashOfTransaction.status) {
          let state = await this.insertToDataBase(owner, hashOfTransaction.out, platform, type);
          resolove({
            status: state,
            error: []
          })
        }
        else {
          resolove({
            status: false,
            error: [this.global.messages.getError('transaction', 'notImplemented')]
          })
        }
      }
      else {
        resolove({
          status: false,
          error: [this.global.messages.getError('transaction', 'canNotExecuteTransaction')]
        })
      }
      
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = send;