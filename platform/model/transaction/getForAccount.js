const MODULE_NAME = 'MODEL getTransactionForAccount';

function getTransactionForAccount() {
  return new Promise(async (resolove, reject) => {
    try {
      let id = await this.global.account.getIdFromSession();
      
      this.global.db.query('SELECT * FROM transactions WHERE whose=? LIMIT 20;',[id])
      .then((result, fields) => {

        if(result.length != 0)
          resolove(result);
        else
          resolove([]);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'SELECT * FROM transactions' 
        })
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = getTransactionForAccount;