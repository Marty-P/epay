const MODULE_NAME = 'MODEL getStatuses';

function getStatuses() {
  return new Promise(async (resolove, reject) => {
    try {
      let id = await this.global.account.getIdFromSession();
      
      this.global.db.query('SELECT * FROM transactions_statuses;',[])
      .then((result, fields) => {

        resolove(result);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'SELECT * FROM transactions' 
        })
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = getStatuses;