const MODULE_NAME = 'MODEL getAddressFromPrivateKay';

function getAddressFromPrivateKay(privateKey) {
  return new Promise(async (resolove, reject) => {
    try {

      let account = await this.global.web3.eth.accounts.privateKeyToAccount(privateKey);

      resolove(account.address)
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = getAddressFromPrivateKay;