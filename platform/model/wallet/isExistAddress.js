const MODULE_NAME = 'MODEL wallet - isExistAddress';

function isExistAddress(address) {
  return new Promise((resolove, reject) => {
    try {
      this.global.db.query('SELECT * FROM wallets WHERE address=?', [address])
      .then((result, fields) => {
        if(result.length == 0)
          resolove(false);
        else
          resolove(true);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: SELECT id FROM wallets'
        })
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isExistAddress;