function Wallets(global) {
  this.global = global;
}

Wallets.prototype.create = require('./create');
Wallets.prototype.insertAddress = require('./insertAddress');

Wallets.prototype.isCorrectPassword = require('./isCorrectPassword');
Wallets.prototype.isCorrectAddress = require('./isCorrectAddress');
Wallets.prototype.isExistAddress = require('./isExistAddress');

Wallets.prototype.getBalance = require('./getBalance');
Wallets.prototype.getIdByAddress = require('./getIdByAddress');
Wallets.prototype.getAddressFromPrivateKey = require('./getAddressFromPrivateKey');

module.exports = Wallets;