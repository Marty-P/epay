const MODULE_NAME = 'MODEL wallet - isCorrectPassword';

function isCorrectPassword(password, repeatPassword) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];

      if((password == undefined) || (repeatPassword == undefined) ) {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'undefinedFields')); 
      }
      else if((password == "") || (repeatPassword == "") ) {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'undefinedFields')); 
      }
      else if(password != repeatPassword) {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'notEqualPassword')); 
      }
      else if(password.length < 8) {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'easyPassword'));
      }

      resolove({
        isValid: isValid,
        value: password,
        errors: errors
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectPassword;