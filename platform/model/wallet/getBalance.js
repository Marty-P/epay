const MODULE_NAME = 'MODEL getBalanceOfWallet';

function getBalance(address, unit) {
  return new Promise(async (resolove, reject) => {
    try {
      let errors = [];

      let balance = await this.global.web3.eth.getBalance(address);

      if(unit != undefined)
        balance = await this.global.web3.utils.fromWei(balance, unit);

      resolove({
        status: true,
        errors: errors,
        value: balance,
        unit: unit || 'wei'
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = getBalance;