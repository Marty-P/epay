const MODULE_NAME = 'MODEL wallet - isCorrectAddress';

function isCorrectAddress(addressSrc) {
  return new Promise((resolove, reject) => {
    try {
      let isValid = true;
      let errors = [];
      let address = '';

      if(addressSrc == undefined) {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'undefinedAddress')); 
      }
      else if(addressSrc == "") {
        isValid = false;
        errors.push(this.global.messages.getError('wallet', 'undefinedAddress')); 
      }
      else {
        let checkAddress = addressSrc.match(/0x[A-Za-z0-9]{40}/);

        if(checkAddress == null)
          isValid = false;
        else if(checkAddress[0] != checkAddress.input)
          isValid = false;
        
        if(!isValid)
          errors.push(this.global.messages.getError('wallet', 'notCorrectAddress'));
        else
          address = this.global.common.getEscapeStr(addressSrc);

      }

      resolove({
        isValid: isValid,
        value: address,
        errors: errors
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = isCorrectAddress;