const MODULE_NAME = 'MODEL wallet - getIdByAddress';

function getIdByAddress(address) {
  return new Promise((resolove, reject) => {
    try {
      this.global.db.query('SELECT id FROM wallets WHERE address=?', [address])
      .then((result, fields) => {
        if(result.length == 0)
          resolove(1);
        else
          resolove(result[0].id);
      })
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: SELECT id FROM wallets'
        })
      })
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = getIdByAddress;