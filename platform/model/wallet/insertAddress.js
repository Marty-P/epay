const MODULE_NAME = 'MODEL wallet - insertAddress';

function insertAddress(address) {
  return new Promise(async (resolove, reject) => {
    try {

      await this.global.db.query('INSERT INTO wallets (address) VALUES (?);', [address])
      .catch((error) => {
        reject({
          error: error,
          module: MODULE_NAME,
          comment: 'db query: INSERT INTO wallets (address)'
        })
      });

      resolove(this.isExistAddress(address));
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try'
      })
    }
  })
}

module.exports = insertAddress;