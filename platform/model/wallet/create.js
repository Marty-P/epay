const MODULE_NAME = 'MODEL createWallet';

function createWallet(passwordSrc, repeatPasswordSrc) {
  return new Promise(async (resolove, reject) => {
    try {
      let errors = [];

      let password = await this.isCorrectPassword(passwordSrc, repeatPasswordSrc);
      errors = errors.concat(password.errors);

      if(password.isValid) {
        let wallet = await this.global.web3.eth.accounts.create();
        let keyStone = await this.global.web3.eth.accounts.encrypt(wallet.privateKey, password.value);

        resolove({
          status: true,
          errors: errors,
          address: wallet.address,
          privateKey: wallet.privateKey,
          keyStone: keyStone
        });
      }
      else {
        resolove({
          status: false,
          errors: errors,
          address: "",
          privateKey: "",
          keyStone: ""
        })
      }

      
    }
    catch(error) {
      reject({
        error: error,
        module: MODULE_NAME,
        comment: 'global try' 
      })
    }
  });
}

module.exports = createWallet;