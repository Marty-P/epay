var express = require('express');
var router = express.Router();

router.use(require('./pay'));
router.use(require('./premium'));
router.use(require('./framePay'));

module.exports = router