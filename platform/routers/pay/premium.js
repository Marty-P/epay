const MODULE_NAME = "ROUTER main";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/buypremium', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized()) {
      response.locals.varTPL = await models.constructor.complexBuild('buyPremium','');

      response.render('entry');
    }
    else
      response.redirect(models.address + '/account/singIn'); 

  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

router.post('/buypremium', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized()) {
      let premium = await models.pay.premium(request.body.privateKey, request.body.period, 300);
      console.log(premium);
      if(premium.status) { 
        response.locals.varTPL = await models.constructor.complexBuild('buyPremium','',{
          errors: premium.errors
        });

        response.render('entry');
      }
      else {
        response.redirect(models.address + '/'); 
      }  
    }
    else
      response.redirect(models.address + '/account/singIn'); 
  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

module.exports = router;