const MODULE_NAME = "ROUTER framePay";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/framePay', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    response.locals.varTPL = await models.constructor.complexBuild('pay', '', {
      id: request.query.id
    });


    response.render('integration');
  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

router.post('/framePay', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    let staticPayment = await models.pay.staticPayment(request.query.id, request.body.private_key, request.body.total);

    if(staticPayment.status)
      response.locals.varTPL = await models.constructor.complexBuild('pay', 'completed', {
        id: request.query.id,
      });
    else
      response.locals.varTPL = await models.constructor.complexBuild('pay', '', {
        id: request.query.id,
        errors: staticPayment.errors
      });


    response.render('integration');
  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

module.exports = router;