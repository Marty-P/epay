const MODULE_NAME = "ROUTER singUp";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/singUp', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized())
      response.redirect(models.address + '/feed');
    else {
      response.locals.varTPL = await models.constructor.simpleBuild({
        page: 'account',
        subpage: 'singUp',
        title: 'singUp'
      });
      response.render('entry');
    }

  }
  catch(error) {
    response.send(error);
  }
});

router.post('/singUp', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized())
      response.redirect(models.address + '/feed');
    else {
      let singUp = await models.account.singUp(request.body.username, request.body.password,  request.body.email);

      if(singUp.status)
        response.locals.varTPL = await models.constructor.message('', 'Регистрация', 
                                                models.messages.getInfo('account', 'checkMailBox'));
      else {
        response.locals.varTPL = await models.constructor.simpleBuild({
          page: 'account',
          subpage: 'singUp',
          title: 'singUp',
          errors: singUp.errors
        });
      }

      response.render('entry');
    }
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;