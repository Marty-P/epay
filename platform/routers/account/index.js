var express = require('express');
var router = express.Router();

router.use(require('./singIn'));
router.use(require('./singUp'));
router.use(require('./singOut'));
router.use(require('./activate'));

module.exports = router