const MODULE_NAME = "ROUTER singOut";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.all('/singOut', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    await models.account.singOut();

    response.redirect(models.address + '/account/singIn');
  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

module.exports = router;