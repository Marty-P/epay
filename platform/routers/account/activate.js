const MODULE_NAME = "ROUTER activate";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.all('/activate', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    let activate = models.account.activate(request.query.key);
    
    response.locals.varTPL = await models.constructor.simpleBuild({
      page: 'account',
      subpage: 'activate',
      title: 'Activation',
      errors: activate.errors
    });
    response.render('entry');
    

  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

module.exports = router;