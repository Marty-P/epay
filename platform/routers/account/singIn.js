const MODULE_NAME = "ROUTER singIn";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/singIn', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized())
      response.redirect(models.address + '/');
    else {
      response.locals.varTPL = await models.constructor.simpleBuild({
        page: 'account',
        subpage: 'singIn',
        title: 'SingIn'
      });
      response.render('entry');
    }

  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/singIn', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized())
      response.redirect(models.address + '/');
    else {
      let singIn = await models.account.singIn(request.body.email, request.body.password);

      if(singIn.status)
        response.redirect(models.address + '/');
      else {
        response.locals.varTPL = await models.constructor.simpleBuild({
          page: 'account',
          subpage: 'singIn',
          title: 'SingIn',
          errors: singIn.errors
        });
        response.render('entry');
      }
    }
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;