const MODULE_NAME = "ROUTER createWallet";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/createWallet', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    let verifiedAccess = false;
    let setOnPlatform = false;
    if(request.query.platform == undefined) {
      verifiedAccess = await models.account.isAuthorized();
      setOnPlatform = false;
    }
    else {
      verifiedAccess = await models.platform.isAccessed(request.query.platform);
      setOnPlatform = true;
    }
      
    if(verifiedAccess){
      response.locals.varTPL = await models.constructor.complexBuild('createWallet','', {
        setOnPlatform: setOnPlatform
      });
      response.render('entry');
    }
    else
      response.redirect(models.address + '/account/singIn'); 

  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/createWallet', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    let verifiedAccess = false;
    let setOnPlatform = false;
    if(request.query.platform == undefined) {
      verifiedAccess = await models.account.isAuthorized();
      setOnPlatform = false;
    }
    else {
      verifiedAccess = await models.platform.isAccessed(request.query.platform);
      setOnPlatform = true;
    }

    if(verifiedAccess) {

      let createWallet = await models.wallet.create(request.body.password, request.body.repeatPassword);
      let setedAddressOnPlatform = false;
      
      if(createWallet.status) {

        //если установлен фложок об установке адреса
        if(request.body.setPlatform == 'on') {
          let setWallet = await models.platform.setWallet(request.query.platform, createWallet.address);
          setedAddressOnPlatform = setWallet.status;
        }

        response.locals.varTPL = await models.constructor.complexBuild('createWallet','created', {
          address: createWallet.address,
          privateKey: createWallet.privateKey,
          keyStone: createWallet.keyStone,
          setedAddressOnPlatform: setedAddressOnPlatform
        });

      }
      else
        response.locals.varTPL = await models.constructor.complexBuild('createWallet','', {
          errors: createWallet.errors,
          setOnPlatform: setOnPlatform
        });
      
      response.render('entry');

    }
    else
      response.redirect(models.address + '/account/singIn'); 
    
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;