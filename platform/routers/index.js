var express = require('express');
var router = express.Router();

//ПРОБЛЕМА С ДВУМЯ ЗАПРОСАМИ ИЗ-ЗА РОУТЕРА СУДЯ ПО ВСЕМУ

router.use('/account', require('./account'));
router.use(require('./platform'));
router.use(require('./wallet'));
router.use(require('./pay'));
router.use('/', require('./main'));
router.use(require('./test'));


router.all('/*', (request, response) =>{
    response.redirect('/'); 
});

module.exports = router