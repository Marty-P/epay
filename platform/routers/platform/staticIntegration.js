const MODULE_NAME = "ROUTER staticIntegration";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/staticIntegration', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);
    

      response.locals.varTPL = await models.constructor.complexBuild('staticIntegration', '', {
        id: request.query.id
      });

      response.render('entry');
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/staticIntegration', async (request, response) => {
  try {
    
    if(await models.platform.isAccessed(request.query.platform)) {
      response.locals.varTPL = await models.constructor.complexBuild('staticIntegration', '');
      response.render('entry');
    }
    else
      response.redirect(models.address + '/account/singIn'); 
    
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;