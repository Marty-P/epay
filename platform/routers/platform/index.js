var express = require('express');
var router = express.Router();

router.use(require('./add'));
router.use(require('./platform'));
router.use(require('./setWallet'));
router.use(require('./staticIntegration'))
router.use(require('./newStaticIntegration'));

module.exports = router