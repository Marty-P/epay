const MODULE_NAME = "ROUTER newStaticIntegration";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/newStaticIntegration', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.platform.isAccessed(request.query.platform)) {
      response.locals.varTPL = await  models.constructor.complexBuild('newStaticIntegration', '');
      response.render('entry');
    }
    else
      response.redirect(models.address + '/'); 

  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/newStaticIntegration', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.platform.isAccessed(request.query.platform)) {
      let newStaticIntegration = await models.platform.newStaticIntegration(request.body, request.query.platform);
      
      if(!newStaticIntegration.status) {
        response.locals.varTPL = await  models.constructor.complexBuild('newStaticIntegration', '',{
          errors: newStaticIntegration.errors
        });
        response.render('entry');
      }
      else
        response.redirect(models.address + '/platform?id=' + request.query.platform);
    }
    else
      response.redirect(models.address + '/');
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;