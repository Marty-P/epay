const MODULE_NAME = "ROUTER addPlatform";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/addPlatform', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized()) {
      response.locals.varTPL = await models.constructor.complexBuild('addPlatform', '');
      response.render('entry');
    }
    else
      response.redirect(models.address + '/account/singIn'); 
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/addPlatform', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized()) {
      let addPlatform = await models.platform.add(request.body.title, request.body.discription, request.body.type);

      if(addPlatform.status)
        response.redirect(models.address + '/');
      else {
        response.locals.varTPL = await models.constructor.complexBuild('addPlatform', '', {
          errors: addPlatform.errors
        });
        response.render('entry');
      }
    }
    else
      response.redirect(models.address + '/account/singIn'); 
    
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;