const MODULE_NAME = "ROUTER platform";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/platform', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.platform.isAccessed(request.query.id)) {
      response.locals.varTPL = await  models.constructor.complexBuild('platform','',{
        id: request.query.id
      });

      response.render('entry');
    }
    else
      response.redirect(models.address + '/'); 
  }
  catch(error) {
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/platform', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.platform.isAccessed(request.query.id)) {
      /////
    }
    else
      response.redirect(models.address + '/'); 
    
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;