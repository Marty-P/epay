const MODULE_NAME = "ROUTER setWallet";

const MODEL = require('../../model');
var express = require('express');
var router = express.Router();

router.get('/setWallet', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);
  
    if(await models.platform.isAccessed(request.query.platform)) {
      response.locals.varTPL = await  models.constructor.complexBuild('setWalletOnPlatform', '', {
        id: request.query.platform
      });

      response.render('entry');
    }
    else
      response.redirect(models.address + '/'); 
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
  
});

router.post('/setWallet', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.platform.isAccessed(request.query.platform)) {
      let setWallet = await models.platform.setWallet(request.query.platform, request.body.address);

      if(setWallet.status)
        response.redirect(models.address + '/platform?=' + request.query.platform);
      else {
        response.locals.varTPL = await  models.constructor.complexBuild('setWalletOnPlatform', '', {
          id: request.query.platform,
          errors: setWallet.errors
        });
        response.render('entry');
      }
    }
    else
      response.redirect(models.address + '/'); 
    
  }
  catch(error) {
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                                models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }

});

module.exports = router;