const MODULE_NAME = "ROUTER main";

const MODEL = require('../model');
var express = require('express');
var router = express.Router();

router.all('/', async (request, response) => {
  try {
    let models = new MODEL(core, request.session);

    if(await models.account.isAuthorized()) {
      response.locals.varTPL = await models.constructor.complexBuild('feed', '');
    }
    else
      response.locals.varTPL = await models.constructor.simpleBuild();

    response.render('entry');
  }
  catch(error){
    console.log(error);
    response.locals.varTPL = await models.constructor.message('error', 'Ошибка выполнения', 
                                          models.messages.getError('global', 'weHaveProblem'));
    response.render('entry');
  }
});

module.exports = router;