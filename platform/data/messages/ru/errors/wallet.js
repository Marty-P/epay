module.exports = {
  undefinedFields: 'Все поля должны быть заполнены',
  notEqualPassword: 'Введенные пароли не идентичны',
  undefinedAddress: 'Адрес не может быть пустым',
  notCorrectAddress: 'Адрес указан не корректно',
  cantSetAddress: 'Невозможно установить платформе этот кошелек',
  noAccess: 'У вас нет доступа',
  easyPassword: 'Пароль должен содержать не менее 8 символов'
}