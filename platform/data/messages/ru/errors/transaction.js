module.exports = {
  notImplemented: 'Транзакция не выполнена',
  canNotExecuteTransaction: 'Невозможно выполнить транзакцию',
  unknowError: 'Трензакия проведена, но у нас возникли сложности'
}