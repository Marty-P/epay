module.exports = {
  undefinedSecretKey: 'Приватный ключ должен быть указан',
  notCorrectSecretKey: 'Приватный ключ указан не корректно',
  notCorrectPeriod: 'Некорректное значение периода',
  undefinedTotal: 'Сумма для перевода должна быть указана',
  notCorrectTotal: 'Не корректное значение суммы для перевода',
  insufficientFunds: 'Не достаточно средств для совершения транзакции',
  notCorrectTotal: 'Не корректное значение суммы для перевода'
}