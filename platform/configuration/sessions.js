const lifetime = 1000*60*60;

module.exports = {
  path: '/',
  secret: 'MkGa)%7DY2', //N1K0*8PS1)
  resave: false,
  saveUninitialized: true,
  store: null,
  cookie: {
    maxAge: lifetime,
    httpOnly: false
  }
}