const lifetime = 1000*60*60;

module.exports = {
  host: 'localhost',
  port: 3306,
  user: 'epay-sessions',
  password: '1234567890sessions',
  database: 'epay-sessions',
  expiration: lifetime,
  createDatabaseTable : true,
  schema: {
    tableName: 'custom_sessions_table_name',
    columnNames: {
      session_id: 'custom_session_id',
      expires: 'custom_expires_column_name',
      data: 'custom_data_column_name'
    }
  }
}