module.exports = {
  server: require('./server'),
  serverEMail: require('./serverEmail'),
  serverMySQL: require('./serverMySQL'),
  sessions: require('./sessions'),
  sessionsStore: require('./sessionsStore'),
  gethServer: 'http://192.168.1.120:4589',
  forntServer: 'http://127.0.0.1:3000',
}