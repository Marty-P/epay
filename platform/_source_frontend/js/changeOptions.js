window.changeOptions = function (operation) {
  let lastId = parseInt(document.querySelector("#numOpts").value);
  switch(operation) {
    case '+':
      if(lastId < 5) {
        let nextId = lastId + 1;
        document.querySelector("#numOpts").value = nextId;
        let input = document.createElement('input');
        input.setAttribute('id', 'opt' + nextId.toString());
        input.setAttribute('name', 'opt' + nextId.toString());
        input.setAttribute('placeholder',  nextId.toString() + ' значение в Wei');
        input.setAttribute('class', 'input-field input-field-opt');
        document.querySelector("#option-fills-area").appendChild(input);
      }
      break;
    case '-':
      if(lastId > 2) {
        document.querySelector("#opt" + lastId.toString()).remove();
        document.querySelector("#numOpts").value =  lastId - 1;
      }
      break;
  }

}