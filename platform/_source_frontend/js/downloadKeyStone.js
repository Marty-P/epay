window.downloadKeyStone = function (){
  var type = 'data:application/octet-stream;base64, ';
  var keyStone =  document.querySelector("#keystone").value;
  var base = btoa(keyStone);
  document.getElementById('downloadLink').href = type + base;
}