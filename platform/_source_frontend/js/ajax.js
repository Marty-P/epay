export function ajax(method, url, body, cookie){
    return new Promise((resolove, reject) =>{
        var xhr = new XMLHttpRequest();
        var bodyReq = body || "";
        var cookieReq = cookie || ""
        xhr.open(method, url); 
        if(method == "POST")
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(bodyReq)
        xhr.onreadystatechange = () =>{
            if(xhr.readyState == XMLHttpRequest.DONE){
                resolove(xhr.responseText);
            }
        }
    });
}