window.selectItem = function (id) {
  if(id == 'payment-options'){

    switch(document.querySelector("#"+id).value) {
      case 'fix':
        document.querySelector('#fix').style.display = 'initial';
        document.querySelector('#options').style.display = 'none';
        document.querySelector('#custom').style.display = 'none';
        break;
      case 'options':
        document.querySelector('#fix').style.display = 'none';
        document.querySelector('#options').style.display = 'initial';
        document.querySelector('#custom').style.display = 'none';
        break;
      case 'custom':
        document.querySelector('#fix').style.display = 'none';
        document.querySelector('#options').style.display = 'none';
        document.querySelector('#custom').style.display = 'initial';
        break;
      default:
        document.querySelector('#fix').style.display = 'initial';
        document.querySelector('#options').style.display = 'none';
        document.querySelector('#custom').style.display = 'none';
    }

  }
  else
    document.querySelector("#"+id).disabled = !document.querySelector("#"+id).disabled;
}