const EXPRESS = require('express');
const BODY_PARSER = require('body-parser');
const SESSIONS = require('express-session');
const SESSIONS_MYSQL = require('express-mysql-session')(SESSIONS);

const CONFIG = require('./configuration')
const CORE = require('./core');

core = new CORE(CONFIG);
let configSessions = CONFIG.sessions;
configSessions.store = new SESSIONS_MYSQL(CONFIG.sessionsStore);

app = EXPRESS(); 
app.use(SESSIONS(configSessions));
app.use(BODY_PARSER.urlencoded({extended: true}));
app.use(EXPRESS.static('./template/static'));
app.use(require('./routers'));

app.set('views', './template');
app.set('view engine', 'ejs');

app.listen(CONFIG.server.port, CONFIG.server.host);